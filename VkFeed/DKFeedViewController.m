//
//  DKFeedViewController.m
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 03.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import "DKFeedViewController.h"
#import <VK-ios-sdk/VKSdk.h>
#import <NHBalancedFlowLayout/NHBalancedFlowLayout.h>
#import "DKFeedViewCell.h"
#import "DK_VKManager.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDImageCache.h>

#import <Reachability/Reachability.h>
#import <TWMessageBarManager/TWMessageBarManager.h>

#import "Reachability+DKReachabler.h"

static NSString *const feedCellIdentifier = @"Cell";

@interface DKFeedViewController () <UITableViewDataSource, UITableViewDelegate, DKFeedViewCellDataSource>

@property (nonatomic, strong) DKFeedViewCell *feedCell;

@property (nonatomic, strong) NSArray *posts;
@property (nonatomic, assign) NSInteger indexOpened;

@end

@implementation DKFeedViewController

#pragma mark - Lifecycle

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        _posts = [NSArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
        initWithTitle:@"Logout" style:UIBarButtonItemStyleDone target:self action:@selector(logout:)];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]
        initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil];
    
    [self setupRefreshControl];
    
    [[SDImageCache sharedImageCache] setMaxCacheAge:7 * 24 * 60 * 60];
    
    _posts = [[DK_VKManager sharedInstance] postsFromCoreDataWithOffset:0];
    
    [self reloadPostsWithOffset:0 andBlock:^{
        [self.tableView reloadData];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [Reachability addReachablerForController:self withSelector:@selector(connectionReachability:)];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    if (!reachability.isReachable) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self connectHandlerWithReachability:reachability];
        });
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [Reachability removeReachablerForController:self];
}

#pragma mark IBActions

- (void)logout:(id)sender {
    [VKSdk forceLogout];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)refreshNewsWall:(id)sender {
    [self reloadPostsWithOffset:0 andBlock:^{
        [self.refreshControl endRefreshing];
        [self.tableView reloadData];
    }];
}

#pragma mark - PRIVATE

#pragma mark Reachability
- (void)connectionReachability:(NSNotification *)reachabilityNotification {
    [self connectHandlerWithReachability:reachabilityNotification.object];
}

- (void)connectHandlerWithReachability:(Reachability *)reachability {
    NSString *title = nil;
    NSString *description = nil;
    TWMessageBarMessageType type = 0;
    
    if (reachability.isReachable) {
        title = @"Connected";
        description = @"You are connected to the Internet!";
        type = TWMessageBarMessageTypeSuccess;
        [self setupRefreshControl];
    } else {
        title = @"Error";
        description = @"You are not connected to the Internet, please connection and try again.";
        type = TWMessageBarMessageTypeError;
        [self deleteRefreshControl];
    }
    [[TWMessageBarManager sharedInstance] hideAllAnimated:YES];
    [[TWMessageBarManager sharedInstance] showMessageWithTitle:title
                                                   description:description
                                                          type:type
                                                      duration:60
                                                statusBarStyle:UIStatusBarStyleLightContent
                                                      callback:nil];
}

#pragma mark RefreshControl methods
- (void)setupRefreshControl {
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(refreshNewsWall:) forControlEvents:UIControlEventValueChanged];
}

- (void)deleteRefreshControl {
    [self.refreshControl removeFromSuperview];
    self.refreshControl = nil;
}

#pragma mark Others
- (void)reloadPostsWithOffset:(NSInteger)offset andBlock:(void (^)())block {
    DK_VKManager *manager = [DK_VKManager sharedInstance];
    [manager postsLoadWithOffset:offset successBlock:^(NSArray *posts) {
        _posts = [posts copy];
        block();
    }];
}

- (NSInteger)indexForTableViewCell:(UITableViewCell *)cell {
    NSInteger index;
    if ([cell isEqual:self.feedCell]) {
        index = self.feedCell.tag;
    } else {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        index = indexPath.row;
    }
    return index;
}

#pragma mark - UITableView delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.feedCell) {
        self.feedCell = [tableView dequeueReusableCellWithIdentifier:feedCellIdentifier];
    }

    DK_VKMPost *post = _posts[indexPath.row];
    
    [self.feedCell.contentLabel setText:[post text]];
    
    // Устанавливаем индекс для ячейки которая не будет показана в TableView для вычислени высоты ячейки.
    self.feedCell.tag = indexPath.row;
    
    [self.feedCell.collectionView reloadData];
    
    [self.feedCell layoutIfNeeded];
    
    CGFloat height = [self.feedCell.contentView
                      systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    return height;
}

#pragma mark - UITableView datasource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DKFeedViewCell *cell = [tableView dequeueReusableCellWithIdentifier:feedCellIdentifier forIndexPath:indexPath];
    
    DK_VKMPost *post = _posts[indexPath.row];
    
    //Date
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSString *dateText = [formatter stringFromDate:[post timestamp]];
    
    //Photo url
    NSURL *photoURL = [NSURL URLWithString:[post photoLink]];
    
    [cell.contentLabel setText:[post text]];
    [cell setCommentsCount:[post commentsCount]];
    [cell setLikesCount:[post likesCount]];
    [cell.userName setText:[post fullName]];
    [cell setIsLiked:[post userIsLiked]];
    [cell.postDate setText:dateText];
    [cell.userPicture setImageWithURL:photoURL];

    [cell.collectionView reloadData];
    
    NSLog(@"CALL cellForRowAtIndexPath");
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_posts count];
}

#pragma mark - DKFeedViewCell datasource/delegate

-(NSInteger)numberOfImagesForFeedViewCell:(DKFeedViewCell *)feedViewCell {
    NSInteger index = [self indexForTableViewCell:feedViewCell];
    DK_VKMPost *post = _posts[index];
    return [post.photoAttachments count];
}

- (DKImageCollectionViewCell *)feedViewCell:(DKFeedViewCell*)feedViewCell collectionViewCellForFeedCellAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *const cellIdentifier = @"imageCell";
    
    DKImageCollectionViewCell *cell = [feedViewCell.collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSInteger index = [self indexForTableViewCell:feedViewCell];
    DK_VKMPost *post = _posts[index];
    DK_VKMPhoto *photo = post.photoAttachments[indexPath.row];
    
    [cell.imageView setImageWithURL:[NSURL URLWithString:photo.link]];
    
    return cell;
}

-(CGSize)feedViewCell:(DKFeedViewCell *)feedViewCell preferredSizeForImageAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = [self indexForTableViewCell:feedViewCell];
    DK_VKMPost *post = _posts[index];
    DK_VKMPhoto *photo = post.photoAttachments[indexPath.row];
    return CGSizeMake(photo.width, photo.height);
}

- (void)feedViewCell:(DKFeedViewCell *)feedViewCell didSelectImageItemAtIndexPath:(NSIndexPath *)indexPath {
//    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
//    browser.displayActionButton = YES;
//    browser.displayNavArrows = NO;
//    browser.displaySelectionButtons = NO;
//    browser.zoomPhotosToFill = YES;
//    browser.alwaysShowControls = NO;
//    browser.enableGrid = NO;
//    browser.startOnGrid = NO;
//    [browser setCurrentPhotoIndex:indexPath.row];
//    
//    NSInteger index = [self indexForTableViewCell:feedViewCell];
//    [self setIndexOpened:index];
//    
//    [self.navigationController pushViewController:browser animated:YES];
}

- (void)feedViewCellLikePostEvent:(DKFeedViewCell *)feedViewCell {
    NSInteger index = [self indexForTableViewCell:feedViewCell];
    DK_VKMPost *post = _posts[index];
    
    [[DK_VKManager sharedInstance] actionLikePostById:post.post_id byOwnerId:post.owner_id withResultBlock:^(NSInteger likesCount, BOOL isUserLike) {
        
        [feedViewCell setIsLiked:isUserLike];
        [feedViewCell setLikesCount:likesCount];
        
        [post setUserIsLiked:isUserLike];
        [post setLikesCount:likesCount];
    }];
    
    NSLog(@"Like :%d post",index);
}

//#pragma mark - MWPhotoBrowser delegate
//
//- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
//    DK_VKMPost *post = _posts[_indexOpened];
//    return [post.photoAttachments count];
//}
//- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
//    DK_VKMPost *post = _posts[_indexOpened];
//    DK_VKMPhoto *photo = post.photoAttachments[index];
//    
//    [MWPhoto photoWithURL:[NSURL URLWithString:photo.link]];
//    
//    if (index < [post.photoAttachments count]) {
//        return [MWPhoto photoWithURL:[NSURL URLWithString:photo.link]];
//    }
//    return nil;
//}
//
//-(void)photoBrowserDidCloseBrowser:(MWPhotoBrowser *)photoBrowser {
//    _indexOpened = 0;
//}



@end
