//
//  DKImageCollectionViewCell.h
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 04.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DKImageCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UIImageView *imageView;

@end
