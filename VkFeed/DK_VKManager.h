//
//  DK_VKManager.h
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 06.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DK_VKMPost.h"

@interface DK_VKManager : NSObject

+ (DK_VKManager *)sharedInstance;

- (NSArray *)postsFromCoreDataWithOffset:(NSInteger)offset;

- (void)postsLoadWithOffset:(NSInteger)offset successBlock:(void (^)(NSArray *posts))successBlock;

- (void)actionLikePostById:(NSInteger)Id byOwnerId:(NSInteger)ownerId withResultBlock:(void (^)(NSInteger likesCount, BOOL isUserLike))resultBlock;

@end

@interface DK_VKManager (VKManagerAccessibility)

+ (NSString *)stringDateFromUnixtime:(NSInteger)time dateFormat:(NSString *)format;

@end
