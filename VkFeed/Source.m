//
//  Source.m
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 12.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import "Source.h"
#import "Photo.h"
#import "Post.h"


@implementation Source

@dynamic source_id;
@dynamic photos;
@dynamic posts;

@end
