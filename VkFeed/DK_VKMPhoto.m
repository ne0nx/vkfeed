//
//  DK_VKMPhoto.m
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 09.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import "DK_VKMPhoto.h"
#import "Photo.h"

@implementation DK_VKMPhoto

+ (instancetype)photoFromPhotoModel:(Photo *)photo
{
    DK_VKMPhoto *photoObject = [self new];
    [photoObject setLink:[photo.link copy]];
    [photoObject setWidth:[photo.width integerValue]];
    [photoObject setHeight:[photo.height integerValue]];
    
    return photoObject;
}

@end
