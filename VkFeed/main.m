//
//  main.m
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 03.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DKAppDelegate class]));
    }
}