//
//  DKFeedViewCell.m
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 03.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import "DKFeedViewCell.h"

#import <NHBalancedFlowLayout/NHBalancedFlowLayout.h>
#import "DKImageCollectionViewCell.h"
#import <SDWebImage/SDWebImageDecoder.h>

//default "like" and "comment" button width
const CGFloat DKFeedViewCellCounterButtonsWidth = 40.f;

@interface DKFeedViewCell () <NHBalancedFlowLayoutDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIButton *likesButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentButtonWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likesButtonWidthConstraint;

@end

@implementation DKFeedViewCell {
    struct {
        unsigned int likePostEvent : 1;
        unsigned int didSelectImageItemAtIndexPath : 1;
    } _dataSourceRespondsTo;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    NHBalancedFlowLayout *layout = (NHBalancedFlowLayout *)self.collectionView.collectionViewLayout;
    layout.minimumLineSpacing = layout.minimumInteritemSpacing = 2;
    layout.sectionInset = UIEdgeInsetsMake(2, 2, 2, 2);
    layout.preferredRowSize = 90.f;
    
    [self.collectionView reloadData];
    
    self.userPicture.layer.cornerRadius = CGRectGetHeight(self.userPicture.frame) / 2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDataSource:(id<DKFeedViewCellDataSource>)dataSource {
    if (_dataSource != dataSource) {
        _dataSource = dataSource;
        _dataSourceRespondsTo.likePostEvent = [self.dataSource respondsToSelector:@selector(feedViewCellLikePostEvent:)];
        _dataSourceRespondsTo.didSelectImageItemAtIndexPath = [self.dataSource respondsToSelector:@selector(feedViewCell:didSelectImageItemAtIndexPath:)];
    }
}

-(void)setLikesCount:(NSInteger)likesCount {
    if (_likesCount != likesCount) {
        _likesCount = likesCount;
        
        [self setCount:likesCount
              inButton:[self likesButton]
         withConstaint:[self likesButtonWidthConstraint]];
    }
}

-(void)setCommentsCount:(NSInteger)commentsCount {
    if (_commentsCount != commentsCount) {
        _commentsCount = commentsCount;
        
        [self setCount:commentsCount
              inButton:[self commentButton]
         withConstaint:[self commentButtonWidthConstraint]];
    }
}

- (void)setIsLiked:(BOOL)isLiked {
    _isLiked = isLiked;
    self.likesButton.selected = isLiked;
}

- (void)setCount:(NSInteger)count inButton:(UIButton *)button withConstaint:(NSLayoutConstraint *)constaint {
    constaint.constant = DKFeedViewCellCounterButtonsWidth;
    
    if (count != 0) {
        NSString *countString = [@(count) stringValue];
        CGRect rect = [countString boundingRectWithSize:
                       CGSizeMake(CGFLOAT_MAX, button.bounds.size.height)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:@{NSFontAttributeName : button.titleLabel.font}
                                                context:nil];
        if (count != 0) {
            constaint.constant += rect.size.width;
        }
        
        [button setTitle:countString forState:UIControlStateNormal];
        
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
    } else {
        [button setTitle:nil forState:UIControlStateNormal];
        
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}

- (void)layoutIfNeeded {
    [super layoutIfNeeded];
    self.collectionViewHeightConstraint.constant = [self.collectionView.collectionViewLayout collectionViewContentSize].height;
}

#pragma mark - NHBalancedFlowLayoutDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(NHBalancedFlowLayout *)collectionViewLayout preferredSizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.dataSource feedViewCell:self preferredSizeForImageAtIndexPath:indexPath];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.dataSource feedViewCell:self didSelectImageItemAtIndexPath:indexPath];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.dataSource numberOfImagesForFeedViewCell:self];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.dataSource feedViewCell:self collectionViewCellForFeedCellAtIndexPath:indexPath];
}

#pragma mark - Action

- (IBAction)commentAction:(id)sender {
    /* 
     * Nothing
     */
}

- (IBAction)likeAction:(id)sender {
    if (_dataSourceRespondsTo.likePostEvent) {
        [self.dataSource feedViewCellLikePostEvent:self];
    }
}

@end
