//
//  Post.h
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 12.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Photo, Source;

@interface Post : NSManagedObject

@property (nonatomic, retain) NSNumber * comments_count;
@property (nonatomic, retain) NSNumber * likes_count;
@property (nonatomic, retain) NSNumber * post_id;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSDate * timestamp;
@property (nonatomic, retain) NSNumber * user_like;
@property (nonatomic, retain) NSOrderedSet *photos;
@property (nonatomic, retain) Source *source;
@end

@interface Post (CoreDataGeneratedAccessors)

- (void)insertObject:(Photo *)value inPhotosAtIndex:(NSUInteger)idx;
- (void)removeObjectFromPhotosAtIndex:(NSUInteger)idx;
- (void)insertPhotos:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removePhotosAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInPhotosAtIndex:(NSUInteger)idx withObject:(Photo *)value;
- (void)replacePhotosAtIndexes:(NSIndexSet *)indexes withPhotos:(NSArray *)values;
- (void)addPhotosObject:(Photo *)value;
- (void)removePhotosObject:(Photo *)value;
- (void)addPhotos:(NSOrderedSet *)values;
- (void)removePhotos:(NSOrderedSet *)values;
@end
