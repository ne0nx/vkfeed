//
//  DK_VKMPhoto.h
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 09.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Photo;

@interface DK_VKMPhoto : NSObject

@property (nonatomic, strong) NSString *link;
@property (nonatomic, assign) NSInteger width;
@property (nonatomic, assign) NSInteger height;

+ (instancetype)photoFromPhotoModel:(Photo *)photo;

@end
