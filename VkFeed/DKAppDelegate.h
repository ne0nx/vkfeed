//
//  DKAppDelegate.h
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 03.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
