//
//  DKAuthViewController.m
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 03.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import "DKAuthViewController.h"
#import <VK-ios-sdk/VKSdk.h>

#import <Reachability/Reachability.h>
#import "Reachability+DKReachabler.h"

#import <TWMessageBarManager/TWMessageBarManager.h>

@interface DKAuthViewController () <VKSdkDelegate>
@end

@implementation DKAuthViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [VKSdk initializeWithDelegate:self andAppId:@"4395818"];
    
    if ([VKSdk wakeUpSession]) {
        [self gotoFeed];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [Reachability addReachablerForController:self withSelector:@selector(connectionReachability:)];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    if (!reachability.isReachable) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self connectHandlerWithReachability:reachability];
        });
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [Reachability removeReachablerForController:self];
}

#pragma mark IBActions

- (IBAction)authorization:(id)sender {
    [VKSdk authorize:@[VK_PER_WALL, VK_PER_FRIENDS] revokeAccess:YES forceOAuth:YES inApp:YES display:VK_DISPLAY_IOS];
}

#pragma mark - PRIVATE

#pragma mark Reachability
- (void)connectionReachability:(NSNotification *)reachabilityNotification {
    [self connectHandlerWithReachability:reachabilityNotification.object];
}

- (void)connectHandlerWithReachability:(Reachability *)reachability {
    NSString *title = nil;
    NSString *description = nil;
    TWMessageBarMessageType type = 0;
    
    if (reachability.isReachable) {
        title = @"Connected";
        description = @"You are connected to the Internet!";
        type = TWMessageBarMessageTypeSuccess;
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
    } else {
        title = @"Error";
        description = @"You are not connected to the Internet, please connection and try again.";
        type = TWMessageBarMessageTypeError;
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
    }
    
    [[TWMessageBarManager sharedInstance] hideAllAnimated:YES];
    [[TWMessageBarManager sharedInstance] showMessageWithTitle:title
                                                   description:description
                                                          type:type
                                                      duration:60
                                                statusBarStyle:UIStatusBarStyleLightContent
                                                      callback:nil];
}

#pragma mark OTHER

- (void)gotoFeed {
    [self performSegueWithIdentifier:@"toFeed" sender:self];
}

#pragma mark - VKsdk delegate

-(void)vkSdkReceivedNewToken:(VKAccessToken *)newToken {
    [self gotoFeed];
}

-(void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {
    [self authorization:nil];
}

-(void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
	[vc presentIn:self];
}

-(void)vkSdkUserDeniedAccess:(VKError *)authorizationError
{
    [[[UIAlertView alloc] initWithTitle:nil message:@"Access denied" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
}

@end
