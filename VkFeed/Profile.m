//
//  Profile.m
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 12.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import "Profile.h"


@implementation Profile

@dynamic first_name;
@dynamic last_name;
@dynamic profile_id;

@end
