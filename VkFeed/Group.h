//
//  Group.h
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 12.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Source.h"


@interface Group : Source

@property (nonatomic, retain) NSNumber * group_id;
@property (nonatomic, retain) NSString * name;

@end
