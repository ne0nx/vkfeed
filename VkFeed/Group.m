//
//  Group.m
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 12.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import "Group.h"


@implementation Group

@dynamic group_id;
@dynamic name;

@end
