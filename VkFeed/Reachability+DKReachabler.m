//
//  Reachability+DKReachabler.m
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 12.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import "Reachability+DKReachabler.h"

@implementation Reachability (DKReachabler)

+ (Reachability *)addReachablerForController:(UIViewController *)viewController withSelector:(SEL)selector
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [[NSNotificationCenter defaultCenter] addObserver:viewController
                                             selector:selector
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    [reachability startNotifier];
    return reachability;
}

+ (void)removeReachablerForController:(UIViewController *)viewController
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [[NSNotificationCenter defaultCenter] removeObserver:viewController
                                                    name:kReachabilityChangedNotification
                                                  object:nil];
    [reachability stopNotifier];
}

@end
