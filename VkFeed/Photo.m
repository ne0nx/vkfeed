//
//  Photo.m
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 12.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import "Photo.h"
#import "Post.h"
#import "Source.h"


@implementation Photo

@dynamic link;
@dynamic width;
@dynamic height;
@dynamic post;
@dynamic source;

@end
