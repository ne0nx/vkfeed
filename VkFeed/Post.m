//
//  Post.m
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 12.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import "Post.h"
#import "Photo.h"
#import "Source.h"


@implementation Post

@dynamic comments_count;
@dynamic likes_count;
@dynamic post_id;
@dynamic text;
@dynamic timestamp;
@dynamic user_like;
@dynamic photos;
@dynamic source;

@end
