//
//  DK_VKManager.m
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 06.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import "DK_VKManager.h"
#import <VK-ios-sdk/VKSdk.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <KCOrderedAccessorFix/NSManagedObjectModel+KCOrderedAccessorFix.h>

#import <SDWebImage/SDWebImageDownloader.h>

#import "DK_VKMPost.h"

#import "Profile.h"
#import "Group.h"
#import "Post.h"
#import "Photo.h"

static NSString *const DK_VKM_ID_KEY        = @"id";
static NSString *const DK_VKM_PHOTO50_KEY   = @"photo_50";
static NSString *const DK_VKM_PHOTO100_KEY  = @"photo_100";
static NSString *const DK_VKM_PHOTO200_KEY  = @"photo_200";

static NSString *const DK_VKM_GROUP_NAME_KEY    = @"name";

static NSString *const DK_VKM_METHOD_DELETE = @"delete";
static NSString *const DK_VKM_METHOD_ADD = @"add";

static NSString *const DK_VKM_LIKE_METHOD_ISLIKED = @"isLiked";

@interface DK_VKManager ()

@property (copy, nonatomic) NSString *nextFrom;

@end

@implementation DK_VKManager

+ (DK_VKManager *)sharedInstance {
    static DK_VKManager *_instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [self new];
    });
    
    return _instance;
}

#pragma mark - PUBLIC

#pragma mark Post

- (NSArray *)postsFromCoreDataWithOffset:(NSInteger)offset { 
    NSFetchRequest *request = [Post MR_requestAll];
    
    NSSortDescriptor *timestampSort = [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:NO];
    [request setSortDescriptors:@[timestampSort]];
    
    request.fetchOffset = offset;
    request.fetchLimit = 30;
    
    //Transformation posts
    NSArray *posts = [Post MR_executeFetchRequest:request];
    //posts = [Post MR_findAll];
    NSMutableArray *array = [NSMutableArray array];
    for (Post *post in posts) {
        DK_VKMPost *mPost = [DK_VKMPost postFromPostModel:post];
        [array addObject:mPost];
    }
    
    return [array copy];
}

- (void)postsLoadWithOffset:(NSInteger)offset successBlock:(void (^)(NSArray *posts))successBlock {
    [self p_loadNewsFeedWithCompletionBlock:^{
        successBlock([self postsFromCoreDataWithOffset:offset]);
    }];
}

#pragma mark Like

- (void)actionLikePostById:(NSInteger)Id byOwnerId:(NSInteger)ownerId withResultBlock:(void (^)(NSInteger likesCount, BOOL isUserLike))resultBlock {
    [self p_isLikedByPostId:Id byOwnerId:ownerId withResultBlock:^(BOOL isLiked) {
        if (isLiked) {
            [self p_deletelikeByPostId:Id byOwnerId:ownerId withResultBlock:^(NSInteger likesCount) {
                resultBlock(likesCount, NO);
            }];
        } else {
            [self p_addlikeByPostId:Id byOwnerId:ownerId withResultBlock:^(NSInteger likesCount) {
                resultBlock(likesCount, YES);
            }];
        }
    }];
}

#pragma mark - PRIVATE

#pragma mark Post

- (void)p_loadNewsFeedWithCompletionBlock:(void (^)())completionBlock {
    NSMutableDictionary *dictionary = [@{@"filters" : @"post", @"count" : @30 } mutableCopy];
    
//    if (_nextFrom != nil) {
//        [dictionary setObject:_nextFrom forKey:@"start_from"];
//    }
    
    VKRequest * getNewsfeed = [VKRequest requestWithMethod:@"newsfeed.get" andParameters:dictionary andHttpMethod:@"GET"];
    
    [getNewsfeed executeWithResultBlock:^(VKResponse *response) {
        
        [self p_savePostsToCoreDataFromDictionary:response.json];
        
        if (completionBlock) {
            completionBlock();
        }
        
    } errorBlock:^(NSError *error) {
        if (error.code != VK_API_ERROR) {
            [error.vkError.request repeat];
        } else {
            NSLog(@"VK error: %@", error);
        }
    }];
}

- (void)p_savePostsToCoreDataFromDictionary:(NSDictionary *)dictionary {
    [dictionary[@"items"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        Post *post = [Post MR_findFirstByAttribute:@"post_id" withValue:obj[VK_API_POST_ID]];
        BOOL postWasFound = NO;
        if (post) {
            postWasFound = YES;
        } else {
            post = [Post MR_createEntity];
        }
        [[[post entity] managedObjectModel] kc_generateOrderedSetAccessors];
        
        VKLikes *likes = [[VKLikes alloc] initWithDictionary:obj[@"likes"]];
        
        post.post_id        = obj[VK_API_POST_ID];
        post.text           = obj[@"text"];
        post.likes_count    = likes.count;
        post.comments_count = obj[@"comments"][VK_API_COUNT];
        post.user_like      = likes.user_likes;
        post.timestamp      = [NSDate dateWithTimeIntervalSince1970:[obj[@"date"] integerValue]];
        
        if (!postWasFound) {
            NSInteger source_id = [obj[@"source_id"] integerValue];
            if (source_id < 0)  { //Community
            
                //Make positive number
                source_id *= -1;
                
                Group *currentGroup = [Group MR_findFirstByAttribute:@"group_id" withValue:@(source_id)];
                if (currentGroup) {
                    [post setSource:currentGroup];
                } else {
                    Source *source = [self p_createObjectWithClass:[Group class] fromArray:dictionary[@"groups"] forId:source_id];
                    [post setSource:source];
                }
            }
            else { //Profile
                
                Profile *currentProfile = [Profile MR_findFirstByAttribute:@"profile_id"
                                                                 withValue:@(source_id)];
                if (currentProfile) {
                    [post setSource:currentProfile];
                } else {
                    Source *source = [self p_createObjectWithClass:[Profile class] fromArray:dictionary[@"profiles"] forId:source_id];
                    [post setSource:source];
                }
            }
            
            if (obj[VK_API_ATTACHMENTS]) {
                [self p_insertAttachments:obj[VK_API_ATTACHMENTS] forPost:post];
            }
        }
    }];
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    //Save next page for request
    [self setNextFrom:dictionary[@"next_from"]];
}

- (void)p_insertAttachments:(NSArray *)attachments forPost:(Post *)post {
    [attachments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj[@"type"] isEqualToString:VK_API_PHOTO]) {
            VKPhoto *vkPhoto = [[VKPhoto alloc] initWithDictionary:obj[VK_API_PHOTO]];
            
            Photo *photo = [Photo MR_createEntity];
            [photo setLink:[vkPhoto photo_604]];
            [photo setWidth:[vkPhoto width]];
            [photo setHeight:[vkPhoto height]];
            
            [post insertObject:photo inPhotosAtIndex:[post.photos count]];
        } else if ([obj[@"type"] isEqualToString:@"video"]) {
            //realize
        } else {
            //realize
        }
    }];
}

- (Source *)p_createObjectWithClass:(Class)class fromArray:(NSArray *)array forId:(NSInteger)identifier {
    NSAssert(class != [Group class] || class != [Profile class], @"undefined class");
    __block Source *source = nil;
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj[@"id"] integerValue] == identifier) {
            if (class == [Group class]) {
                source = [self p_storeGroupFromDictionary:obj];
                *stop = YES;
            } else if (class == [Profile class]) {
                source = [self p_storeProfileFromDictionary:obj];
                *stop = YES;
            }
        }
    }];
    return source;
}

- (Profile *)p_storeProfileFromDictionary:(NSDictionary *)dictionary {
    VKUser *user = [[VKUser alloc] initWithDictionary:dictionary];
    
    Profile *profile = [Profile MR_createEntity];
    
    //Fix accessors methods
    [[[profile entity] managedObjectModel] kc_generateOrderedSetAccessors];
    
    [profile setProfile_id:[user id]];
    [profile setFirst_name:[user first_name]];
    [profile setLast_name:[user last_name]];
    [profile setSource_id:dictionary[DK_VKM_ID_KEY]];
    
    NSArray *photos = [NSArray arrayWithObjects:user.photo_50, user.photo_100, user.photo_200, nil];
    
    [photos enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Photo *photo = [Photo MR_createEntity];
        photo.link = obj;
        [profile insertObject:photo inPhotosAtIndex:idx];
    }];
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    return profile;
}

- (Group *)p_storeGroupFromDictionary:(NSDictionary *)dictionary {
    Group *group = [Group MR_createEntity];

    //Fix accessors methods
    [[[group entity] managedObjectModel] kc_generateOrderedSetAccessors];
    
    group.group_id       = dictionary[DK_VKM_ID_KEY];
    group.name           = dictionary[DK_VKM_GROUP_NAME_KEY];
    group.source_id      = @(-[dictionary[DK_VKM_ID_KEY] integerValue]);
    
    NSArray *photos = [NSArray arrayWithObjects:dictionary[DK_VKM_PHOTO50_KEY], dictionary[DK_VKM_PHOTO100_KEY], dictionary[DK_VKM_PHOTO200_KEY], nil];
    
    [photos enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
         Photo *photo = [Photo MR_createEntity];
         photo.link = obj;
         [group insertObject:photo inPhotosAtIndex:idx];
    }];
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    return group;
}

#pragma mark - Likes

- (void)p_addlikeByPostId:(NSInteger)Id byOwnerId:(NSInteger)ownerId withResultBlock:(void (^)(NSInteger likesCount))resultBlock {
    [self p_likeRequestWithMethod:DK_VKM_METHOD_ADD type:@"post" byId:Id byOwnerId:ownerId withResultBlock:^(NSDictionary *json) {
        resultBlock([json[@"likes"] integerValue]);
    }];
}
- (void)p_deletelikeByPostId:(NSInteger)Id
                   byOwnerId:(NSInteger)ownerId
             withResultBlock:(void (^)(NSInteger likesCount))resultBlock {
    [self p_likeRequestWithMethod:DK_VKM_METHOD_DELETE type:@"post" byId:Id byOwnerId:ownerId withResultBlock:^(NSDictionary *json) {
        resultBlock([json[@"likes"] integerValue]);
    }];
}

- (void)p_isLikedByPostId:(NSInteger)Id
                byOwnerId:(NSInteger)ownerId
          withResultBlock:(void (^)(BOOL isLiked))resultBlock {
    [self p_likeRequestWithMethod:DK_VKM_LIKE_METHOD_ISLIKED type:@"post" byId:Id byOwnerId:ownerId withResultBlock:^(NSDictionary *json) {
        resultBlock([json[@"liked"] boolValue]);
    }];
}

- (void)p_likeRequestWithMethod:(NSString *)method type:(NSString *)type byId:(NSInteger)Id byOwnerId:(NSInteger)ownerId
                withResultBlock:(void (^)(NSDictionary *json))resultBlock {
    VKRequest *likeReq = [VKRequest requestWithMethod:[NSString stringWithFormat:@"likes.%@",method] andParameters:@{@"type": type, @"item_id" : @(Id), @"owner_id" : @(ownerId)} andHttpMethod:@"GET"];
    [likeReq executeWithResultBlock:^(VKResponse *response) {
        resultBlock(response.json);
    } errorBlock:^(NSError *error) {
        if (error.code != VK_API_ERROR) {
            [error.vkError.request repeat];
        } else {
            NSLog(@"VK error: %@", error);
        }
    }];
}

@end

@implementation DK_VKManager (VKManagerAccessibility)

+ (NSString *)stringDateFromUnixtime:(NSInteger)time dateFormat:(NSString *)format {
    NSDateFormatter *_formatter= [NSDateFormatter new];
    [_formatter setLocale:[NSLocale currentLocale]];
    [_formatter setDateFormat:format];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time];
    return [_formatter stringFromDate:date];
}

@end
