//
//  DKFeedViewCell.h
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 03.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DKFeedViewCellDataSource;

#import "DKImageCollectionViewCell.h"

@interface DKFeedViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userPicture;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *postDate;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic) NSInteger commentsCount;

@property (nonatomic) BOOL isLiked;
@property (nonatomic) NSInteger likesCount;

@property (nonatomic, strong) IBOutlet id<DKFeedViewCellDataSource> dataSource;

@end


@protocol DKFeedViewCellDataSource <NSObject>

@required
- (NSInteger)numberOfImagesForFeedViewCell:(DKFeedViewCell*)feedViewCell;
- (DKImageCollectionViewCell *)feedViewCell:(DKFeedViewCell*)feedViewCell collectionViewCellForFeedCellAtIndexPath:(NSIndexPath *)indexPath;
- (CGSize)feedViewCell:(DKFeedViewCell*)feedViewCell preferredSizeForImageAtIndexPath:(NSIndexPath *)indexPath;

//- (UIImage *)feedViewCell:(DKFeedViewCell *)feedViewCell imageForCellAtIndexPath:(NSIndexPath *)indexPath;

@optional
- (void)feedViewCellLikePostEvent:(DKFeedViewCell*)feedViewCell;
-(void)feedViewCell:(DKFeedViewCell *)feedViewCell didSelectImageItemAtIndexPath:(NSIndexPath *)indexPath;

@end