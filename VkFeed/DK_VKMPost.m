//
//  DK_VKMPost.m
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 07.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import "DK_VKMPost.h"

#import "Post.h"
#import "Source.h"
#import "Group.h"
#import "Profile.h"
#import "Photo.h"

@implementation DK_VKMPost

+ (instancetype)postFromPostModel:(Post *)post {
    DK_VKMPost *postObject = [self new];
    [postObject setPost_id:[post.post_id integerValue]];
    [postObject setText:[post.text copy]];
    [postObject setLikesCount:[post.likes_count integerValue]];
    [postObject setCommentsCount:[post.comments_count integerValue]];
    [postObject setTimestamp:[post.timestamp copy]];
    [postObject setUserIsLiked:[post.user_like boolValue]];
    
    if ([post.source isKindOfClass:[Group class]]) {
        __weak Group *group = (Group *)post.source;
        
        postObject.fullName = group.name;
        postObject.owner_id = -[group.group_id integerValue];
    } else if ([post.source isKindOfClass:[Profile class]]) {
        __weak Profile *profile = (Profile *)post.source;
        
        postObject.owner_id = [profile.profile_id integerValue];
        postObject.fullName = [NSString stringWithFormat:@"%@ %@",
                           profile.first_name,
                           profile.last_name];
    } else {
        NSAssert(YES, @"post.source: undefined class");
    }
    
    postObject.photoLink = [[(Photo *)[post.source.photos firstObject] link] copy];
    postObject.photoAttachments = [postObject p_photosFromPost:post];

    return postObject;
}

- (NSArray *)p_photosFromPost:(Post *)post {
    NSMutableArray *array = [NSMutableArray array];
    [post.photos enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [array addObject:[DK_VKMPhoto photoFromPhotoModel:obj]];
    }];
    return [array copy];
}

@end
