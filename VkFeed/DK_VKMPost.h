//
//  DK_VKMPost.h
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 07.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DK_VKMPhoto.h"

@class Post;

@interface DK_VKMPost : NSObject

//Post data
@property (nonatomic, assign) NSInteger post_id;
@property (nonatomic, assign) NSInteger owner_id;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, assign) NSInteger likesCount;
@property (nonatomic, assign) NSInteger commentsCount;
@property (nonatomic, strong) NSDate * timestamp;
@property (nonatomic, assign) BOOL userIsLiked;

//Source data
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *photoLink;

//Photos
@property (nonatomic, strong) NSArray *photoAttachments;

+ (instancetype)postFromPostModel:(Post *)post;

@end
