//
//  Photo.h
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 12.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Post, Source;

@interface Photo : NSManagedObject

@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSNumber * width;
@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) Post *post;
@property (nonatomic, retain) Source *source;

@end
