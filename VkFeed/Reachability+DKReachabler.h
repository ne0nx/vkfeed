//
//  Reachability+DKReachabler.h
//  VkFeed
//
//  Created by Dmitriy Karachentsov on 12.06.14.
//  Copyright (c) 2014 smob.su. All rights reserved.
//

#import "Reachability.h"

@interface Reachability (DKReachabler)

+ (Reachability *)addReachablerForController:(UIViewController *)viewController
                                withSelector:(SEL)selector;
+ (void)removeReachablerForController:(UIViewController *)viewController;

@end
